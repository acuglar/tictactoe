//Diretrizes:

//Por turno considere:
//1. Qual player joga?
//2. Quais células já selecionadas?
//3. Há alguma combinação vencedora (ou seja, 3-em-linha)?


//##Configurando o Estado Inicial do Jogo##

let currentPlayer = 'X';
let nextPlayer = 'O';

let playerXSelections = [];
let playerOSelections = [];

const winningCombinations = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9],
    [1, 4, 7],
    [2, 5, 8],
    [3, 6, 9],
    [1, 5, 9],
    [3, 5, 7]
];


//##Detectando Quando uma Célula é Clicada##

//function handleClick registra o ID da célula que foi clicada.
const handleClick = function (event) {
    const cell = event.target
/*     console.log(cell.id);
 */

    //##Jogabilidade##

    //imprime o caractere do jogador atual (X ou O) no tabuleiro e alternar o jogador.
    cell.innerHTML = currentPlayer;

    if (currentPlayer === 'X') {
        playerSelections = playerXSelections;
        nextPlayer = 'O';
    } else {
        playerSelections = playerOSelections;
        nextPlayer = 'X';
    }

    playerSelections.push(Number(cell.id));

    if (checkWinner(playerSelections)) {
        alert('Player ' + currentPlayer + ' wins!');
        resetGame();
    }

    if (checkDraw()) {
        alert('Draw!');
        resetGame();
    }

    // Troca jogadores
    currentPlayer = nextPlayer;

}


//Obtém um array de todas as células    
const cells = document.querySelectorAll('td');

//Itera as células para adicionar um event listener
for (let i = 0; i < cells.length; i++) {
    cells[i].addEventListener('click', handleClick);
}


//##Verificando a combinação de vitória##


function checkWinner() { 
    
    // Verifica para cada combinação se o jogador tem todos os valores
    for (let i = 0; i < winningCombinations.length; i++) {
        let eachWinningCombination = winningCombinations[i];
        let matches = 0;    
        for (let j = 0; j < eachWinningCombination.length; j++) {
            if (eachWinningCombination.includes(playerSelections[j])) {
                matches++
                console.log(matches)
            } else {
                break  // vai para a próxima combinação
            }
            if (matches === 3) {
                return true
            }
        }
    }

    // Se nós percorremos todas as combinações sem retornar true
    // então o jogador não venceu
    return false
}


//##Verificar Empate##

function checkDraw() {
    return (playerOSelections.length + playerXSelections.length) >= cells.length;
}


//##Terminando o Jogo##

function resetGame() {
    playerXSelections = new Array();
    playerOSelections = new Array();
    for (let i = 0; i < cells.length; i++) {
        cells[i].innerHTML = '';
    }
}


/*
JavaScript:

Impeça que os usuários selecionem a mesma célula mais de uma vez (do jeito que está agora, um jogador pode invadir a seleção de outro jogador).
Exiba de quem é a vez.
Registre a quantidade de vitórias de cada jogador (e a quantidade de empates).
CSS:

Use cursor: pointer; para mostrar a conhecida mão com o dedo indicador estendido ao passar o mouse sobre as células.
Deixe seu tabuleiro mais bonito.
Adicione uma animação quando os Xs e Os forem adicionados ao tabuleiro.
*/